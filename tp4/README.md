# TP4 : Real services

## Partie 1 : Partitionnement du serveur de stockage

🌞 Partitionner le disque à l'aide de LVM

    [yanice@storage ~]$ sudo pvcreate /dev/sdb
    [sudo] password for yanice:
    Physical volume "/dev/sdb" successfully created.
    [yanice@storage ~]$ sudo pvcreate /dev/sdc
    Physical volume "/dev/sdc" successfully created.
    [yanice@storage ~]$ sudo pvs
    Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VB84ea0422-ec836fde PVID cgLsJS71F1t3ZGyv9zkSqt1icVzgxe82 last seen on /dev/sda2 not found.
    PV         VG Fmt  Attr PSize PFree
    /dev/sdb      lvm2 ---  2.00g 2.00g
    /dev/sdc      lvm2 ---  2.00g 2.00g
#
    [yanice@storage ~]$ sudo vgcreate storage /dev/sdb
    Volume group "storage" successfully created
    [yanice@storage ~]$ sudo vgextend storage /dev/sdc
    Volume group "storage" successfully extended
    [yanice@storage ~]$ sudo vgs
    Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VB84ea0422-ec836fde PVID cgLsJS71F1t3ZGyv9zkSqt1icVzgxe82 last seen on /dev/sda2 not found.
    VG      #PV #LV #SN Attr   VSize VFree
    storage   2   0   0 wz--n- 3.99g 3.99g
#
    [yanice@storage ~]$ sudo lvcreate -l 100%FREE storage -n mon_storage
    Logical volume "mon_storage" created.

🌞 Formater la partition

    [yanice@storage ~]$ sudo mkfs -t ext4 /dev/storage/mon_storage
    mke2fs 1.46.5 (30-Dec-2021)
    Creating filesystem with 1046528 4k blocks and 261632 inodes
    Filesystem UUID: 0cd41caa-98be-4c76-9328-f98c129f9789
    Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (16384 blocks): done
    Writing superblocks and filesystem accounting information: done

    [yanice@storage ~]$ sudo lvdisplay
    Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VB84ea0422-ec836fde PVID cgLsJS71F1t3ZGyv9zkSqt1icVzgxe82 last seen on /dev/sda2 not found.
     --- Logical volume ---
    LV Path                /dev/storage/mon_storage
    LV Name                mon_storage
    VG Name                storage
    LV UUID                AyqSbD-BYT2-9Uto-FrAT-aoKe-UW5W-W6UKOd
    LV Write Access        read/write
    LV Creation host, time storage.tp4.linux, 2024-02-19 15:51:08 +0100
    LV Status              available
    # open                 0
    LV Size                3.99 GiB
    Current LE             1022
    Segments               2
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     256
    Block device           253:2

🌞 Monter la partition
    
    [yanice@storage ~]$ sudo mkdir /mnt/storage

    [yanice@storage ~]$ mount /dev/storage/mon_storage /mnt/storage/

    [yanice@storage ~]$ df -h | grep storage
    /dev/mapper/storage-mon_storage  3.9G   24K  3.7G   1% /mnt/storage
#
    [yanice@storage ~]$ sudo cat /etc/fstab

    #
    # /etc/fstab
    # Created by anaconda on Tue Oct 24 11:46:00 2023
    #
    # Accessible filesystems, by reference, are maintained under '/dev/disk/'.
    # See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
    #
    # After editing this file, run 'systemctl daemon-reload' to update systemd
    # units generated from this file.
    #
    /dev/mapper/rl-root     /                       xfs     defaults        0 0
    UUID=7c364ee9-cfa9-4d2d-a125-9bd98aabd079 /boot                   xfs     defaults        0 0
    /dev/mapper/rl-swap     none                    swap    defaults        0 0

    /dev/storage/mon_storage /mnt/storage ext4 defaults 0 0

⭐BONUS

    [yanice@storage ~]$ sudo dd if=/dev/random of=/mnt/storage/toto bs=4M count=2000 status=progress
#
    [yanice@storage ~]$ df -h | grep storage
    /dev/mapper/storage-mon_storage  3.9G  3.9G     0 100% /mnt/storage
#
    [yanice@storage ~]$ sudo pvcreate /dev/sdd
    Physical volume "/dev/sdd" successfully created.

    [yanice@storage ~]$ sudo vgextend storage /dev/sdd
    Volume group "storage" successfully extended  

    [yanice@storage ~]$ sudo lvextend -l +100%FREE /dev/storage/mon_storage
    Size of logical volume storage/mon_storage changed from 3.99 GiB (1022 extents) to <5.99 GiB (1533 extents).
    Logical volume storage/mon_storage successfully resized.

    [yanice@storage ~]$ sudo resize2fs /dev/storage/mon_storage
    resize2fs 1.46.5 (30-Dec-2021)
    Filesystem at /dev/storage/mon_storage is mounted on /mnt/storage; on-line resizing required
    old_desc_blocks = 1, new_desc_blocks = 1
    The filesystem on /dev/storage/mon_storage is now 1569792 (4k) blocks long.  
#
    [yanice@storage ~]$ df -h | grep storage
    /dev/mapper/storage-mon_storage  5.9G  3.9G  1.7G  70% /mnt/storage

## Partie 2 : Serveur de partage de fichiers

🌞 Donnez les commandes réalisées sur le serveur NFS storage.tp4.linux

    [yanice@storage ~]$ sudo dnf install nfs-utils

    [yanice@storage ~]$ sudo mkdir /mnt/storage/site_web_1
    [yanice@storage ~]$ sudo mkdir /mnt/storage/site_web_2

    [yanice@storage ~]$ sudo nano /etc/exports
    [yanice@storage ~]$ cat /etc/exports
    /mnt/storage/site_web_1 10.6.1.9(rw,sync,no_subtree_check,no_root_sqash)
    /mnt/storage/site_web_2 10.6.1.9(rw,sync,no_subtree_check,no_root_sqash)

    [yanice@storage ~]$ sudo systemctl enable nfs-server
    [yanice@storage ~]$ sudo systemctl start nfs-server

    [yanice@storage ~]$ sudo firewall-cmd --permanent --list-all | grep services
    services: cockpit dhcpv6-client ssh
    [yanice@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
    success
    [yanice@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
    success
    [yanice@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
    success
    [yanice@storage ~]$ sudo firewall-cmd --reload
    success
    [yanice@storage ~]$ sudo firewall-cmd --permanent --list-all | grep services
    services: cockpit dhcpv6-client mountd nfs rpc-bind ssh

🌞 Donnez les commandes réalisées sur le client NFS web.tp4.linux

    [yanice@web ~]$ sudo mkdir -p /var/www

    [yanice@web ~]$ sudo mount 10.6.1.8:/mnt/storage/site_web_1 /var/www
    [yanice@web ~]$ sudo mount 10.6.1.8:/mnt/storage/site_web_2 /var/www

    [yanice@web ~]$ sudo cat /etc/fstab | grep storage
    10.6.1.8:/mnt/storage/site_web_1 /var/www/site_web_1 nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
    10.6.1.8:/mnt/storage/site_web_2 /var/www/site_web_2 nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

## Partie 3 : Serveur web

### 1. Intro NGINX

### 2. Install

🖥️ VM web.tp4.linux

🌞 Installez NGINX

    sudo dnf install nginx

### 3. Analyse

🌞 Analysez le service NGINX

    [yanice@web ~]$ ps aux | grep nginx
    root       12267  0.0  0.1  10108   956 ?        Ss   14:53   0:00 nginx: master process /usr/sbin/nginx
    nginx      12268  0.0  0.6  13908  4984 ?        S    14:53   0:00 nginx: worker process
    yanice     12270  0.0  0.2   6408  2276 pts/0    S+   14:54   0:00 grep --color=auto nginx  

    [yanice@web ~]$ sudo ss -antpl | grep nginx
    LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=12268,fd=6),("nginx",pid=12267,fd=6))
    LISTEN 0      511             [::]:80            [::]:*    users:(("nginx",pid=12268,fd=7),("nginx",pid=12267,fd=7))

### 4. Visite du service web

🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX

    [yanice@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success

🌞 Accéder au site web

    yanic@PC_de_yanice MINGW64 ~
    $ curl 10.6.1.9:80
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
    0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
    <html>
        <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <title>HTTP Server Test Page powered by: Rocky Linux</title>
            <style type="text/css">
                /*<![CDATA[*/

                html {
                    height: 100%;
                    width: 100%;
                }
                    body {

🌞 Vérifier les logs d'accès

    [yanice@web ~]$ cd /var/log/nginx/access.log

    [yanice@web ~]$ sudo tail -n 3 /var/log/nginx/access.log
    10.6.1.3 - - [20/Feb/2024:15:26:59 +0100] "GET /poweredby.png HTTP/1.1" 200 368 "http://10.6.1.9/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36" "-"
    10.6.1.3 - - [20/Feb/2024:15:26:59 +0100] "GET /favicon.ico HTTP/1.1" 404 3332 "http://10.6.1.9/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36" "-"
    10.6.1.3 - - [20/Feb/2024:15:27:26 +0100] "GET / HTTP/1.1" 200 7620 "-" "curl/8.2.1" "-"

## 5. Modif de la conf du serveur web

🌞 Changer le port d'écoute

    [yanice@web ~]$ sudo cat /etc/nginx/nginx.conf | grep 8080
        listen       8080;
        listen       [::]:8080;

    [yanice@web ~]$ sudo systemctl restart nginx
    [yanice@web ~]$ systemctl status nginx
    ● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; prese>
     Active: active (running) since Tue 2024-02-20 15:34:29 CET; 5s ago

     [yanice@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
    success
    [yanice@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
    success   
    [yanice@web ~]$ sudo firewall-cmd --reload
    success  

    [yanice@web ~]$ curl 10.6.1.9:8080
        <!doctype html>
            <html>
                <head>
                    <meta charset='utf-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1'>
                    <title>HTTP Server Test Page powered by: Rocky Linux</title>
                    <style type="text/css">
                    /*<![CDATA[*/
            html {
                height: 100%;
                width: 100%;
            }
                body {

🌞 Changer l'utilisateur qui lance le service
