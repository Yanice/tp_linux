#!/bin/bash
#yanice
#23/02/2024

#afficher le nom de la machine
echo -n "Nom de la machine : "; hostname

#afficher le nom de l'os
osrelease=$(sudo cat /etc/os-release | grep PRETTY_NAME | cut -d'"' -f2)
kernel=$(uname -r)
echo -e "OS: $osrelease and Kernel version is: $kernel"

#afficher l'adresse IP
echo -n "ip : "; ip a | grep inet | awk '$1 == "inet" {print $2}' | awk 'NR==2'

#afficher l'état de la RAM
ram_total=$(free -h | awk '/^Mem/ {print $2}')
ram_available=$(free -h | awk '/^Mem/ {print $7}')
echo -e "RAM : $ram_available memory available on $ram_total total memory"

#afficher l'espace restant sur le disque
disque=$(df -h | awk 'NR==2 {print $4}')
echo -e "Disque : $disque space left"

#afficher le top 5 des process
echo -e "Top 5 processes by RAM usage :"; ps -eo command= --sort=-%mem | head -n5 | aw>
#afficher la liste des ports en ecoute
echo "Listening ports :"
ss -tln | awk 'NR > 1 {split($4, a, ":"); print "  -", a[2], $1, ":", $NF}'

#afficher image
cat_url=$(curl -s "https://api.thecatapi.com/v1/images/search?api_key=${1}" | cut -d',>file_extension=$(echo "$cat_url" | awk -F. '{print $NF}')
echo -e "Here is your random cat ($file_extension file) : $cat_url"