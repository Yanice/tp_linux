# TP5 : We do a little scripting

## Partie 1 : Script carte d'identité

🌞 Vous fournirez dans le compte-rendu Markdown, en plus du fichier, un exemple d'exécution avec une sortie

    [yanice@yanice idcard]$ ./idcard.sh
    Nom de la machine : yanice
    OS: Rocky Linux 9.2 (Blue Onyx) and Kernel version is: 5.14.0-284.30.1.el9_2.x86_64
    ip : 10.6.1.11/24
    RAM : 467Mi memory available on 759Mi total memory
    Disque : 4.0M space left
    Top 5 processes by RAM usage :
        - python3
        - NetworkManager
        - systemd
        - systemd
        - systemd
    Listening ports :
        - 22 LISTEN : 0.0.0.0:*
        -  LISTEN : [::]:*
    Here is your random cat (jpg file) : https://cdn2.thecatapi.com/images/2ck.jpg

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, un exemple d'exécution avec une sortie

    [yanice@yanice yt]$ ./yt.sh
    Le dossier existe.
    Entrez l'URL de la vidéo : https://www.youtube.com/watch?v=xETF8qkMuwA
    Vidéo https://www.youtube.com/watch?v=xETF8qkMuwA téléchargée
    File path : /srv/yt/downloads/L'OUVRIER A POIL AU TRAVAIL - News succulentes 3/L'OUVRIER A POIL AU TRAVAIL - News succulentes 3.mp4

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :

    [yanice@yanice yt]$ systemctl status yt
    ● yt.service - Service de téléchargement de vidéos YouTube
     Loaded: loaded (/etc/systemd/system/yt.service; enabled; preset: disabled)
     Active: active (running) since Wed 2024-03-13 10:51:06 CET; 1s ago
    Main PID: 1824 (yt-v2.sh)
      Tasks: 2 (limit: 4610)
     Memory: 588.0K
        CPU: 4ms
     CGroup: /system.slice/yt.service
             ├─1824 /bin/bash /srv/yt/yt-v2.sh
             └─1825 sleep 60

    Mar 13 10:51:06 yanice systemd[1]: Started Service de téléchargement de vidéos YouTube.