#!/bin/bash
#yanice
#5/03/2024

#verif que le dossier /srv/yt/downloads existe
if [ -d /srv/yt/downloads ]; then
        echo "Le dossier existe."
else
        echo "Le dossier n'existe pas."
        exit1
fi

# Vérification si le dossier /var/log/yt existe
if [ ! -d /var/log/yt ]; then
    echo "Le dossier /var/log/yt n'existe pas."
    exit 1
fi

#demander l'URL de la vidéo
read -p "Entrez l'URL de la vidéo : " url

#récuperer le nom de la vidéo
video_name=$(youtube-dl --get-title "$url")

#créer le dossier pour la vidéo
if [ ! -e "/srv/yt/downloads/$video_name" ]; then
        mkdir -p "/srv/yt/downloads/$video_name"
        youtube-dl -o "/srv/yt/downloads/$video_name/$video_name.mp4" "$url" >/dev/null        youtube-dl --write-description -o "/srv/yt/downloads/$video_name/description.t>        echo "Vidéo $url téléchargée"
        echo "File path : /srv/yt/downloads/$video_name/$video_name.mp4"
        # Enregistrement de la ligne de log dans /var/log/yt/download.log
        timestamp=$(date +"%Y/%m/%d %T")
        echo "[$timestamp] Video $url was downloaded. File path : /srv/yt/downloads/$v>
else
        echo "Vidéo déja téléchargée"
        exit
fi