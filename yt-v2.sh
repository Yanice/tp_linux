#!/bin/bash
#yanice
#10/03/2024

# Vérification si le dossier /srv/yt/downloads existe
if [ ! -d /srv/yt/downloads ]; then
    echo "Le dossier /srv/yt/downloads n'existe pas."
    exit 1
fi

# Vérification si le dossier /var/log/yt existe
if [ ! -d /var/log/yt ]; then
    echo "Le dossier /var/log/yt n'existe pas."
    exit 1
fi

# Boucle infinie
while true; do
    # Vérifier si le fichier contenant les URLs existe
    if [ -f urls.txt ]; then
        # Lire chaque URL dans le fichier
        while IFS= read -r url || [ -n "$url" ]; do
            # Vérifier si l'URL n'est pas vide
            if [ -n "$url" ]; then
                # Récupérer le nom de la vidéo
                video_name=$(youtube-dl --get-title "$url")
                # Créer le dossier pour la vidéo si nécessaire
                if [ ! -d "/srv/yt/downloads/$video_name" ]; then
                    mkdir -p "/srv/yt/downloads/$video_name"
                fi
                # Vérifier si la vidéo n'a pas déjà été téléchargée
                if [ ! -f "/srv/yt/downloads/$video_name/$video_name.mp4" ]; then
                    # Télécharger la vidéo
                    echo "Téléchargement de la vidéo : $video_name"
                    youtube-dl -o "/srv/yt/downloads/$video_name/$video_name.mp4" "$url" >/dev/null
                    # Enregistrer la ligne de log dans /var/log/yt/download.log
                    timestamp=$(date +"%Y/%m/%d %T")
                    echo "[$timestamp] Video $url was downloaded. File path : /srv/yt/downloads/$video_name/$video_name.mp4" >> /var/log/yt/download.log
                else
                    echo "Vidéo déjà téléchargée : $video_name"
                fi
                # Supprimer l'URL du fichier
                sed -i '1d' urls.txt
            fi
        done < urls.txt
    else
        echo "Le fichier urls.txt n'existe pas."
    fi
    # Attendre X secondes avant de vérifier à nouveau
    sleep 60
done