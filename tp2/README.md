# TP2 : Appréhension d'un système Linux

# Partie 1: Files and users

## I. Fichiers

### 1. Find me

🌞 Trouver le chemin vers le répertoire personnel de votre utilisateur

    [yanice@localhost ~]$ cd //home/yanice/

🌞 Trouver le chemin du fichier de logs SSH

    [yanice@localhost ~]$ cd //var/log/

🌞 Trouver le chemin du fichier de configuration du serveur SSH

    [yanice@localhost ~]$ cd //etc/ssh

## II. Users

### 1. Nouveau user

🌞 Créer un nouvel utilisateur

    [yanice@localhost ~]$ sudo useradd -m -d /home/aper_alu/marmotte -s /bin/bash marmotte

    [yanice@localhost ~]$ sudo passwd marmotte

### 2. Infos enregistrées par le système

🌞 Prouver que cet utilisateur a été créé

    [yanice@localhost ~]$ cat /etc/passwd | grep marmotte
    marmotte:x:1001:1001::/home/aper_alu/marmotte:/bin/bash

🌞 Déterminer le hash du password de l'utilisateur marmotte

    [yanice@localhost ~]$ sudo cat /etc/shadow | grep marmotte
    marmotte:$6$u0wll6LUjtIoYeDG$PishvOL0epK/5fKf0vwDMpHXwqvk9O4ylCNXFA9U.pU.tK4n6YmscaEonf4trbSX4Tqt9qFVu1Fq10cM1qil6.:19744:0:99999:7:::

### 3. Hint sur la ligne de commande

### 3. Connexion sur le nouvel utilisateur

🌞 Tapez une commande pour vous déconnecter : fermer votre session utilisateur

    [yanice@localhost ~]$ exit
    logout
    Connection to 10.6.1.4 closed.

🌞 Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte

    PS C:\Windows\system32> ssh marmotte@10.6.1.4
    marmotte@10.6.1.4's password:

    [marmotte@localhost ~]$ ls //home/yanice/
    ls: cannot open directory '//home/yanice/': Permission denied


# Partie 2 : Programmes et paquets

## I. Programmes et processus

### 1. Run then kill

🌞 Lancer un processus sleep

    [yanice@localhost ~]$ sleep 1000

    [yanice@localhost ~]$ ps -ef | grep sleep
    yanice      1615    1561  0 16:25 pts/0    00:00:00 sleep 1000

🌞 Terminez le processus sleep depuis le deuxième terminal

    [yanice@localhost ~]$ kill 1615

### 2. Tâche de fond

🌞 Lancer un nouveau processus sleep, mais en tâche de fond

    [yanice@localhost ~]$ sleep 1000 &

🌞 Visualisez la commande en tâche de fond

    [yanice@localhost ~]$ ps -ef | grep sleep
    yanice      1714    1695  0 16:31 pts/2    00:00:00 sleep 1000

### 3. Find paths

🌞 Trouver le chemin où est stocké le programme sleep

    [yanice@localhost ~]$ ls //bin | grep sleep
    sleep

🌞 Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent .bashrc

    [yanice@localhost ~]$ sudo find / -name .bashrc
    [sudo] password for yanice:
    /etc/skel/.bashrc
    /root/.bashrc
    /home/yanice/.bashrc
    /home/aper_alu/marmotte/.bashrc

### 4. La variable PATH

🌞 Vérifier que

    [yanice@localhost ~]$ which sleep ssh ping
    /usr/bin/sleep
    /usr/bin/ssh
    /usr/bin/ping
    [1]+  Done                    sleep 1000

## II. Paquets

🌞 Installer le paquet firefox

    [yanice@localhost ~]$ sudo dnf install git

🌞 Utiliser une commande pour lancer Firefox

    [yanice@localhost /]$ which git
    /usr/bin/git

🌞 Installer le paquet nginx

    [yanice@localhost /]$ sudo dnf install nginx

🌞 Déterminer

    [yanice@localhost ~]$ cd //var/log/nginx/

    [yanice@localhost /]$ cd //etc/nginx/

🌞 Mais aussi déterminer...

    [yanice@localhost yum.repos.d]$ grep -rn -E '^mirrorlist'

# Partie 3 : Poupée russe

🌞 Récupérer le fichier meow

    [yanice@localhost ~]$ sudo dnf install wget
    [yanice@localhost yanice]$ wget https://gitlab.com/it4lik/b1-linux-2023/-/raw/master/tp/2/meow

🌞 Trouver le dossier dawa/

    [yanice@localhost yanice]$ file meow
    meow: Zip archive data, at least v2.0 to extract
    [yanice@localhost yanice]$ mv meow meow.zip
    [yanice@localhost yanice]$ unzip meow.zip
    Archive:  meow.zip
    inflating: meow

    [yanice@localhost yanice]$ ls
    meow  meow.zip

    [yanice@localhost yanice]$ file meow
    meow: XZ compressed data
    [yanice@localhost yanice]$ mv meow meow.xz
    [yanice@localhost yanice]$ xz -dk meow.xz

    [yanice@localhost yanice]$ ls
    meow  meow.xz  meow.zip

    [yanice@localhost yanice]$ file meow
    meow: bzip2 compressed data, block size = 900k
    [yanice@localhost yanice]$ mv meow meow.bz2
    [yanice@localhost yanice]$ bzip2 -dk meow.bz2

    [yanice@localhost yanice]$ ls
    meow  meow.bz2  meow.xz  meow.zip

    [yanice@localhost yanice]$ file meow
    meow: RAR archive data, v5
    [yanice@localhost yanice]$ mv meow meow.rar
    [yanice@localhost yanice]$ unrar x meow.rar

    [yanice@localhost yanice]$ ls
    meow  meow.bz2  meow.rar  meow.xz  meow.zip

    [yanice@localhost yanice]$ file meow
    meow: gzip compressed data, from Unix, original size modulo 2^32 145049600
    [yanice@localhost yanice]$ mv meow meow.gz
    [yanice@localhost yanice]$ gzip -dk meow.gz

    [yanice@localhost yanice]$ ls
    meow  meow.bz2  meow.gz  meow.rar  meow.xz  meow.zip

    [yanice@localhost yanice]$ file meow
    meow: POSIX tar archive (GNU)
    [yanice@localhost yanice]$ mv meow meow.tar.gz
    

