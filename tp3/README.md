# TP3 : Services

## I. Service SSH

# 1. Analyse du service

🌞 S'assurer que le service sshd est démarré

    [yanice@localhost ~]$ systemctl status
    ● localhost.localdomain
    State: running

🌞 Analyser les processus liés au service SSH

    [yanice@localhost ~]$ ps aux | grep ssh
    root         685  0.0  1.1  15836  9328 ?        Ss   14:33   0:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
    root        4109  0.0  1.4  18832 11504 ?        Ss   14:35   0:00 sshd: yanice [priv]
    yanice      4113  0.0  0.9  19016  7160 ?        S    14:35   0:00 sshd: yanice@pts/0
    yanice      4169  0.0  0.2   6408  2300 pts/0    S+   14:44   0:00 grep --color=auto ssh

🌞 Déterminer le port sur lequel écoute le service SSH

    [yanice@localhost ~]$ sudo ss -antp | grep ssh
    [sudo] password for yanice:
    LISTEN 0      128          0.0.0.0:22        0.0.0.0:*     users:(("sshd",pid=685,fd=3))
    ESTAB  0      36          10.6.1.5:22       10.6.1.3:64638 users:(("sshd",pid=4113,fd=4),("sshd",pid=4109,fd=4))
    LISTEN 0      128             [::]:22           [::]:*     users:(("sshd",pid=685,fd=4))

🌞 Consulter les logs du service SSH

    [yanice@localhost ~]$ journalctl | grep ssh
    Jan 29 14:33:34 localhost systemd[1]: Created slice Slice /system/sshd-keygen.
    Jan 29 14:33:35 localhost systemd[1]: Reached target sshd-keygen.target.
    Jan 29 14:33:36 localhost sshd[685]: main: sshd: ssh-rsa algorithm is disabled
    Jan 29 14:33:36 localhost sshd[685]: Server listening on 0.0.0.0 port 22.
    Jan 29 14:33:36 localhost sshd[685]: Server listening on :: port 22.
    Jan 29 14:35:27 localhost.localdomain sshd[4109]: main: sshd: ssh-rsa algorithm is disabled
    Jan 29 14:35:35 localhost.localdomain sshd[4109]: Accepted password for yanice from 10.6.1.3 port 64638 ssh2
    Jan 29 14:35:35 localhost.localdomain sshd[4109]: pam_unix(sshd:session): session opened for user yanice(uid=1000) by (uid=0)
# 
    [yanice@localhost ~]$ sudo tail -n 10 /var/log/secure
    Jan 29 15:01:10 localhost sudo[4243]: pam_unix(sudo:session): session closed for user root
    Jan 29 15:01:44 localhost sudo[4248]:  yanice : TTY=pts/0 ; PWD=/home/yanice ; USER=root ; COMMAND=/bin/tail -n 10 /var/log/secure
    Jan 29 15:01:44 localhost sudo[4248]: pam_unix(sudo:session): session opened for user root(uid=0) by yanice(uid=1000)
    Jan 29 15:01:44 localhost sudo[4248]: pam_unix(sudo:session): session closed for user root
    Jan 29 15:01:47 localhost sudo[4252]:  yanice : TTY=pts/0 ; PWD=/home/yanice ; USER=root ; COMMAND=/bin/tail -n 10 /var/log/secure
    Jan 29 15:01:47 localhost sudo[4252]: pam_unix(sudo:session): session opened for user root(uid=0) by yanice(uid=1000)
    Jan 29 15:01:47 localhost sudo[4252]: pam_unix(sudo:session): session closed for user root
    Jan 29 15:01:49 localhost sudo[4256]:  yanice : TTY=pts/0 ; PWD=/home/yanice ; USER=root ; COMMAND=/bin/tail -n 10 /var/log/secure
    Jan 29 15:01:49 localhost sudo[4256]: pam_unix(sudo:session): session opened for user root(uid=0) by yanice(uid=1000)
    Jan 29 15:01:49 localhost sudo[4256]: pam_unix(sudo:session): session closed for user root

# 2. Modification du service

🌞 Identifier le fichier de configuration du serveur SSH

    /etc/ssh/sshd_config

🌞 Modifier le fichier de conf

    [yanice@localhost ~]$ echo $RANDOM /etc/ssh/sshd_config
    23923 /etc/ssh/sshd_config
#
    [yanice@localhost ~]$ sudo nano /etc/ssh/sshd_config
    [yanice@localhost ~]$ sudo cat /etc/ssh/sshd_config | grep Port
    Port 2222
#
    [yanice@localhost ~]$ sudo firewall-cmd --remove-port=22/tcp --permanent
    Warning: NOT_ENABLED: 22:tcp
    success
    [yanice@localhost ~]$ sudo firewall-cmd --add-port=2222/tcp --permanent
    success
    [yanice@localhost ~]$ sudo firewall-cmd --reload
    success
    [yanice@localhost ~]$ sudo firewall-cmd --list-all | grep 2222
    ports: 2222/tcp   

🌞 Redémarrer le service

    [yanice@localhost ~]$ sudo systemctl restart sshd

🌞 Effectuer une connexion SSH sur le nouveau port

    PS C:\Users\yanic> ssh yanice@10.6.1.5 -p 2222
    yanice@10.6.1.5's password:
    Last failed login: Mon Jan 29 15:29:49 CET 2024 from 10.6.1.3 on ssh:notty
    There were 2 failed login attempts since the last successful login.
    Last login: Mon Jan 29 14:35:35 2024 from 10.6.1.3

## II. Service HTTP

# 1. Mise en place

🌞 Installer le serveur NGINX

    [yanice@localhost ~]$ sudo dnf install nginx

🌞 Démarrer le service NGINX

    [yanice@localhost ~]$ sudo systemctl start nginx
    [sudo] password for yanice: 

🌞 Déterminer sur quel port tourne NGINX

    [yanice@localhost ~]$ sudo ss -antp | grep nginx
    LISTEN 0      511          0.0.0.0:80        0.0.0.0:*     users:(("nginx",pid=14444,fd=6),("nginx",pid=14443,fd=6))
    LISTEN 0      511             [::]:80           [::]:*     users:(("nginx",pid=14444,fd=7),("nginx",pid=14443,fd=7))

🌞 Déterminer les processus liés au service NGINX

    [yanice@localhost ~]$ ps aux | grep nginx
    root       14443  0.0  0.1  10108   956 ?        Ss   15:42   0:00 nginx: master process /usr/sbin/nginx
    nginx      14444  0.0  0.6  13908  4972 ?        S    15:42   0:00 nginx: worker process
    yanice     14456  0.0  0.2   6408  2296 pts/0    S+   15:43   0:00 grep --color=auto nginx

🌞 Déterminer le nom de l'utilisateur qui lance NGINX

    [yanice@localhost ~]$ cat /etc/passwd | grep nginx
    nginx:x:991:991:Nginx web server:/var/lib/nginx:/sbin/nologin

🌞 Test !

    yanic@PC_de_yanice MINGW64 ~
    $ curl -I http://10.6.1.5:80 | head -n 7
     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
    0  7620    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    HTTP/1.1 200 OK
    Server: nginx/1.20.1
    Date: Mon, 29 Jan 2024 14:52:33 GMT
    Content-Type: text/html
    Content-Length: 7620
    Last-Modified: Thu, 02 Feb 2023 21:29:03 GMT
    Connection: keep-alive

# 2. Analyser la conf de NGINX

🌞 Déterminer le path du fichier de configuration de NGINX

    [yanice@localhost ~]$ ls -al /etc/nginx/nginx.conf
    -rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf

🌞 Trouver dans le fichier de conf

    [yanice@node1 ~]$ cat /etc/nginx/nginx.conf | grep "server {" -A 10
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
    --
    #    server {
    #        listen       443 ssl http2;
    #        listen       [::]:443 ssl http2;
    #        server_name  _;
    #        root         /usr/share/nginx/html;
    #
    #        ssl_certificate "/etc/pki/nginx/server.crt";
    #        ssl_certificate_key "/etc/pki/nginx/private/server.key";
    #        ssl_session_cache shared:SSL:1m;
    #        ssl_session_timeout  10m;
    #        ssl_ciphers PROFILE=SYSTEM;

# 3. Déployer un nouveau site web

🌞 Créer un site web

    [yanice@node1 ~]$ cat //var/www/tp3_linux/index.html
    <h1>MEOW mon premier serveur web</h1>

🌞 Gérer les permissions

    [yanice@node1 ~]$ sudo chown -R nginx:nginx /var/www/tp3_linux

🌞 Adapter la conf NGINX

    [yanice@node1 ~]$ sudo nano /etc/nginx/conf.d/monsite.conf
    [yanice@node1 ~]$ sudo cat /etc/nginx/conf.d/monsite.conf
    server {
    # le port choisi devra être obtenu avec un 'echo $RANDOM' là encore
    listen 11683;

    root /var/www/tp3_linux;
    }

🌞 Visitez votre super site web

    [yanice@node1 ~]$ sudo firewall-cmd --add-port=11683/tcp
    success
    [yanice@node1 ~]$ curl http://10.6.1.5:11683
    <h1>MEOW mon premier serveur web</h1>

## III. Your own services

# 1. Au cas où vous l'auriez oublié

# 2. Analyse des services existants

🌞 Afficher le fichier de service SSH

    [yanice@node1 ~]$ systemctl status sshd
    ● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service;
#
    [yanice@node1 ~]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
    ExecStart=/usr/sbin/sshd -D $OPTIONS

    [yanice@node1 ~]$ sudo systemctl start sshd

🌞 Afficher le fichier de service NGINX

    [yanice@node1 ~]$ sudo cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
    ExecStart=/usr/sbin/nginx

# 3. Création de service

🌞 Créez le fichier /etc/systemd/system/tp3_nc.service

    [yanice@node1 ~]$ cat /etc/systemd/system/tp3_nc.service
    [Unit]
    Description=Super netcat tout fou

    [Service]
    ExecStart=/usr/bin/nc -l 5592 -k

🌞 Indiquer au système qu'on a modifié les fichiers de service

    [yanice@node1 ~]$ sudo systemctl daemon-reload

🌞 Démarrer notre service de ouf

    [yanice@node1 ~]$ sudo systemctl start tp3_nc.service

🌞 Vérifier que ça fonctionne

    [yanice@node1 ~]$ sudo systemctl status tp3_nc.service
    ● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Tue 2024-01-30 14:38:11 CET; 31s ago
    Main PID: 1714 (nc)
      Tasks: 1 (limit: 4610)
     Memory: 796.0K
        CPU: 11ms
     CGroup: /system.slice/tp3_nc.service
             └─1714 /usr/bin/nc -l 5592 -k

    Jan 30 14:38:11 node1.tp3.b1 systemd[1]: Started Super netcat tout fou.
#
    [yanice@node1 ~]$ ss -anltp |grep 5592
    LISTEN 0      10           0.0.0.0:5592       0.0.0.0:*
# 
    [yanice@node1 ~]$ nc 10.6.1.5 5592
    yanice
    hourcade
    jojo

🌞 Les logs de votre service

    [yanice@node1 ~]$ sudo journalctl -xe -u tp3_nc
    ~
    Jan 30 14:38:11 node1.tp3.b1 systemd[1]: Started Super netcat tout fou.
    ░░ Subject: A start job for unit tp3_nc.service has finished successfully
    ░░ Defined-By: systemd
    ░░ Support: https://access.redhat.com/support
    ░░
    ░░ A start job for unit tp3_nc.service has finished successfully.
    ░░
    ░░ The job identifier is 1860.
    Jan 30 14:46:15 node1.tp3.b1 nc[1714]: yanice
    Jan 30 14:46:17 node1.tp3.b1 nc[1714]: hourcade
    Jan 30 14:46:35 node1.tp3.b1 nc[1714]: jojo
# 
    [yanice@node1 ~]$ sudo journalctl -xe -u tp3_nc | grep Started
    Jan 30 14:38:11 node1.tp3.b1 systemd[1]: Started Super netcat tout fou.
# 
    [yanice@node1 ~]$ sudo journalctl -xe -u tp3_nc | grep -E "nc[[*]"
    Jan 30 14:46:15 node1.tp3.b1 nc[1714]: yanice
    Jan 30 14:46:17 node1.tp3.b1 nc[1714]: hourcade
    Jan 30 14:46:35 node1.tp3.b1 nc[1714]: jojo
#
    [yanice@node1 ~]$ sudo journalctl -xe -u tp3_nc | grep -i stop
    Jan 30 14:56:21 node1.tp3.b1 systemd[1]: Stopping Super netcat tout fou...
    ░░ Subject: A stop job for unit tp3_nc.service has begun execution
    ░░ A stop job for unit tp3_nc.service has begun execution.
    Jan 30 14:56:21 node1.tp3.b1 systemd[1]: Stopped Super netcat tout fou.
    ░░ Subject: A stop job for unit tp3_nc.service has finished
    ░░ A stop job for unit tp3_nc.service has finished.

🌞 S'amuser à kill le processus

    [yanice@node1 ~]$ sudo ps aux | grep nc
    root        1898  0.5  0.3  10256  3024 ?        Ss   15:03   0:00 /usr/bin/nc -l 5592 -k

    [yanice@node1 ~]$ sudo kill 1898    

🌞 Affiner la définition du service

    [yanice@node1 ~]$ cat /etc/systemd/system/tp3_nc.service
    [Unit]
    Description=Super netcat tout fou

    [Service]
    ExecStart=/usr/bin/nc -l 5592 -k
    Restart=always
#
    [yanice@node1 ~]$ sudo systemctl daemon-reload
    [yanice@node1 ~]$ sudo systemctl restart tp3_nc.service
#
    [yanice@node1 ~]$ sudo ps aux | grep nc
    root        1952  0.0  0.3  10256  3028 ?        Ss   15:11   0:00 /usr/bin/nc -l 5592 -k

    [yanice@node1 ~]$ sudo kill 1952

    [yanice@node1 ~]$ sudo ps aux | grep nc
    root        1960  0.0  0.3  10256  3028 ?        Ss   15:11   0:00 /usr/bin/nc -l 5592 -k




