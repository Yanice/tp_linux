# TP_Linux

## TP1 : Casser avant de construire

# I. Setup

# II. Casser

🌞 Supprimer des fichiers

    rm /bin/bash 

🌞 Mots de passe

    cat /etc/shadow

    rm /etc/passwd 

🌞 Another way ?

    nano /etc/passwd

    root:x:0:0:root:/root:reboot
    yanice:x:1000:1000:yanice:/home/yanice:sleep

🌞 Effacer le contenu du disque dur

    dd if=/dev/zero of=/dev/sdX bs=1M

🌞 Trouvez 4 autres façons de détuire la machine

    1. rm -r /boot/loader/entries/